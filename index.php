<?php 


include "vendor/autoload.php";
use DemoComponents\Emailer;
use DemoComponents\ZipFiles;

try { 
    $emailer = new Emailer();
    $zipFiles = new ZipFiles(__DIR__. "/files"); 
    $zipFiles->generateZipArchive(array(
            'files/file1.csv',
            'files/file2.csv',
        ), "file3.zip", "password");
    $template = file_get_contents("Templates/email.php");
    $emailer->addAttachment($zipFiles->getFullPath(), $zipFiles->getFileName());
    $emailer->setTo("adam.j.synnott@gmail.com");
    $emailer->setBcc("adam.j.synnott@gmail.com");
    $emailer->setSubject("Email Report Attached");
    $emailer->setFrom(array('name' => 'Adam Synnott', 'email' => 'adam.j.synnott@ec2-54-79-17-63.ap-southeast-2.compute.amazonaws.com'));
    $emailer->setMessage($template);
    $emailer->sendEmail();
    $zipFiles->removeZipArchive();

} catch( \Exception $exception) {
    echo $exception->getMessage();
}
