<?php
function __autoload($classPath) {
    $filePath = str_replace("\\", "/", $classPath) . ".php";
    include $filePath;
}