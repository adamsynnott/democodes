<?php



include "../vendor/autoload.php";

use DemoComponents\ZipFiles;
use \PHPUnit_Framework_TestCase;

class ZipFilesTest extends PHPUnit_Framework_TestCase {
 
    public function testGetDirectory() {
    
        $directory = __DIR__;
        $zipper = new ZipFiles($directory);
        $zipperDirectory = $zipper->getDirectory();
        $this->assertEquals($directory, $zipperDirectory);

    }
    
    public function testSetDirectory() {
        $directory = __DIR__. "/../files"; 
        $zipper = new ZipFiles($directory);
        try {
            $zipper->setDirectory("aldskj");
        } catch(Exception $exception) {
           return;
        }
        $this->fail('An expected exception has not been raised.');
    }
    
    public function testZipFiles() {
        
        $directory = __DIR__. "/../files"; 
        $zipper = new ZipFiles($directory);
        $success = $zipper->generateZipArchive(array(
            '../files/file1.csv',
            '../files/file2.csv',
        ), "file3.zip", "0r1g1n$");
        
        $this->assertEquals(true, $success);
 
    }
    
    public function testRemoveZipFiles() {
        
        $directory = __DIR__. "/../files"; 
        $zipper = new ZipFiles($directory);
        $zipper->generateZipArchive(array(
            '../files/file1.csv',
            '../files/file2.csv',
        ), "file3.zip", "0r1g1n$");
        
        $success = $zipper->removeZipArchive();
        $this->assertEquals(true, $success);
 
    }
    
}

?>