<?php



include "../vendor/autoload.php";

use DemoComponents\Emailer;
use \PHPUnit_Framework_TestCase;

class EmailerTest extends PHPUnit_Framework_TestCase { 
    
    public function testAddAttachmentFail(){
        $emailer = new Emailer();
        try {
            $emailer->addAttachment("nonesense", "nonesense");
        } catch(Exception $exception) {
            $this->assertTrue($exception->getCode() === 0);
            return;
        }
        
        $this->fail('An expected exception has not been raised.');
        
    }
    
    public function testAddAttachmentSucceed(){
        $emailer = new Emailer();
        
        try {
            $emailer->addAttachment(__FILE__, "nonesense");
        } catch(Exception $exception) {
            $this->fail('An unexpected exception has been raised.');
            return;
        }
        
        $this->assertTrue(true);

    }
    
}