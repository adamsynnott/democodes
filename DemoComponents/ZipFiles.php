<?php
/**
 * ZipFiles
 * @author Adam
 */

namespace DemoComponents;


class ZipFiles {
    
    private $_fileName = false;
    private $_directory = false;

    /**
     * __construct
     * Sets the local directory for saving zip files into
     * @param        string        $directory        the directory to store the zip file
     */
    public function __construct($directory) {
        $this->setDirectory($directory);  
    }
    
    /**
     * getDirectory
     * getter for the directory property
     * @returns       string                          the absolute directory path 
     */
    public function getDirectory() {
        return $this->_directory;
    }
    
    /**
     * setDirectory
     * setter for the directory property
     * @param          string        $directory       sets the directory property
     */
    public function setDirectory($directory) {
        
        $success = false;
        if(file_exists($directory) && is_writable($directory)) {
            $this->_directory = $directory;
            $success = true;
        } 
        if($success) {
            return $success;
        } else {
            throw new \Exception("The specified directory either doesn't exist or is not writable", 0);
        }
  
    }
    
    /**
     * getFileName
     * getter for the fileName property
     * @return         string                         the current filename
     */
    public function getFileName() {
        return $this->_fileName;
    }
    
    /**
     * setFileName
     * setter for the fileName property
     * @param          string          $fileName      the desired file name
     */
    public function setFileName($fileName = "") {
        $this->_fileName = $fileName;
    }
    
    /**
     * getFullFilePath
     * getter for the full file path
     * @returns         string                        the absolute path of the file
     */
    public function getFullPath() {
        if(file_exists($this->_directory. "/" . $this->_fileName)) {
            return $this->_directory. "/" . $this->_fileName;
        }
        return false;
    }
    
    
    /**
     * generateZipArchive
     * Creates the zip archive with an array of file paths optional password protection 
     * @param        array         $files            an array of absolute file paths to add to the zip archive
     * @param        string        $fileName         filename of the zip archive to be created
     * @param        string        $password         optional password protection for the zipArchive
     * @returns      bool
     */
    public function generateZipArchive($files = array(), $fileName, $password = null) {
        
        $success = false;
        $this->_fileName = $fileName;
        $command = $this->buildCommand($files, $fileName, $password);
        system($command, $success);
        $success = $success === 0 ? true : false;
        
        // check if the generated file exists
        if(!$this->getFullPath()) {
            $success = false;
        } 
        
        return $success;
    }
    
    /**
     * removeZipArchive
     * removes the generated zip archive from the directory
     */
    public function removeZipArchive(){
        $success = unlink($this->getFullPath());
        if($success) {
            $this->setFileName();
        }
        return $success;
    }
    
 
    /**
     * buildCommand
     * builds the zip command
     * @param        array         $files            an array of absolute file paths
     * @returns      string                          the zip command
     */
    private function buildCommand($files = array(), $fileName, $password = null) {
        
        $pFlag = is_null($password) ? "" : "-P $password";
        $command = "zip $pFlag {$this->_directory}/$fileName " . implode(" ", $files);
        return $command;
        
    }

  
}