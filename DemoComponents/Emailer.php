<?php
/**
 * Emailer
 * @author Adam
 */
namespace DemoComponents;

class Emailer {
    
    private $headers;
    private $body;
    private $message;
    private $attachments = array();
    private $to = array();
    private $from = array();
    private $replyTo;
    private $Cc = array();
    private $Bcc = array();
    private $subject;
    private $multipartStep;
    
    /**
     * __construct
     * generated the mulitpart step for attachments, set the _from property to default values
     */
    public function __construct() {
        $this->multipartStep = '-----'.md5(time()).'-----';
        $this->setFrom(array(
            "name" => "Emailer",
            "email" => "adam.j.synnott@gmail.com",
        ));
    }

    /**
     * addAttachment
     * adds a file attachment to the attachments array
     * @param             string             $filePath            absolute path of the file            
     * @param             string             $attachmentName      the filename to show in the attachment email
     * @return            boolean
     * @throws            Exception                               only if there is a problemo
     */
    public function addAttachment($filePath = null, $attachmentName = null) {
        
        
        if(!is_null($filePath) && !is_null($attachmentName) && file_exists($filePath)) {
            
            $attachmentFile = file_get_contents($filePath);
            $attachmentData = chunk_split(base64_encode($attachmentFile));
            $mimeType = $this->getMimeType($filePath);
            
            $this->attachments[] = "Content-Type: $mimeType\r\n"
            . "Content-Transfer-Encoding: base64\r\n"
            . "Content-Disposition: attachment; filename=\"$attachmentName\"\r\n"
            . "\r\n"
            . "$attachmentData\r\n";
            
            return true;
            
        } else {
            throw new \Exception("There was a problem attaching the file", 0);
        }

    }
    
    /**
     * setTo
     * setter for the to property
     * @param            mixed            $to         string or array of strings containing email addresses
     */
    public function setTo($to){
        $this->setEmails("to", $to);
    }
    
    /**
     * setTo
     * setter for the from property, also sets reply to property
     * @param            mixed            $from         string or array of strings containing email addresses
     */
    public function setFrom($from = null) { 
        if(!is_null($from)) {
            $this->from = array_replace_recursive($this->from, $from);
        }
        $this->replyTo = $this->from;
       
    }
    
    /**
     * setTo
     * setter for the replyTo property
     * @param            mixed            $replyTo     string or array of strings containing email addresses
     */
    public function setReplyTo($replyTo) {
        if(!is_null($replyTo)) {
            $this->replyTo = array_replace_recursive($this->replyTo, $replyTo);
        }
    }
    
    /**
     * setCc
     * setter for the Cc property
     * @param            mixed            $cc     string or array of strings containing email addresses
     */
    public function setCc($cc){
        $this->setEmails("_Cc", $cc);
    }
    
    
    /**
     * setBcc
     * setter for the Bcc property
     * @param            mixed            $bcc     string or array of strings containing email addresses
     */
    public function setBcc($bcc){
        $this->setEmails("_Bcc", $bcc);
    }
    
    /**
     * setEmails
     * setter for all the email properties
     * @param             string          $type    the name of the address property to set
     * @param             mixed           $data    string or array of strings containing email addresses
     */
    private function setEmails($type, $data) {
        $setType = &$this->$type;
        if(is_array($data)){
            foreach($data as $email) { 
                $setType[] = $email;
            }
        }
        
        if(is_string($data)) {
            $setType[] = $data;
        }

    }
    
    /**
     * setSubject
     * setter for the email subject
     * @param      string       $subject      the email subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * setMessage
     * sets the email message in the correct encoding
     * @param         sting           $message   the message  
     */
    public function setMessage($message) {
        
        $this->message = "Content-type: text/html; charset=utf-8\r\n"
        . "Content-Transfer-Encoding: 8bit\r\n\r\n"
        . "$message\r\n\r\n";
        
    }

    /**
     * sendEmail
     * sends the email using the mail function
     * @returns       bool 
     */
    public function sendEmail() {
       $this->assembleHeaders();
       $this->assembleBody();
       echo implode("\r\n", $this->headers);
       echo $this->body;
       
       return mail('', $this->subject, $this->body, implode("\r\n", $this->headers));
    }
    
    /**
     * getMimeType
     * gets the mime type of an attached file
     * @param         string          $filePath            absolute path of the file  
     * @return        string                                  content type and charset of the file
     */
    private function getMimeType($filePath) {
        $finfo = new \finfo(FILEINFO_MIME, "/usr/share/misc/magic");
        return $finfo->file($filePath);
    }
    
    
    /**
     * assembleHeaders
     * assebmles the various headers than need to be set
     */
    private function assembleHeaders(){
        
        $this->headers = array(
            "To: "  . implode(", ", $this->to),
            "From: {$this->from['name']} <{$this->from['email']}>",
            "MIME-Version: 1.0",
            "Reply-To: " . $this->replyTo['name'] . " <" . $this->replyTo['email'] . ">",
            "Content-Type: multipart/mixed; boundary=\"{$this->multipartStep}\"",
            "Cc: " . implode(", ", $this->Cc),
            "Bcc: " . implode(", ", $this->Bcc),
        );
    }
    
    /**
     * assembleBody
     * assembles the body and attachments with the mulitpart step 
     */
    public function assembleBody() {
        $body = "\r\n";
        foreach($this->attachments as $attachment) {
            $body .= "--$this->multipartStep\r\n" . $attachment;
        }
        $body .= "--$this->multipartStep\r\n"
               . $this->message
               . "--$this->multipartStep--";
        $this->body = $body;
    }
   
}